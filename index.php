<!doctype html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<h1>
<?php

$color = "green";
$x = 5;
$y = 30;

$z = &$x;
$z = 8;
//echo $x;

$age = 7;

const PI = 3.14;

/**
 * Exception
 */
function testExceeption($num) {
    if($num === 0) {
        throw new Exception('Invalid divider');
        echo 'in test';
    }
    return $num;
}

echo 'test start<br>';
    try {
//        testExceeption(0);
    } catch (Exception $e) {
        echo 'Caught exception: ',  $e->getMessage(), "<br>";
    }
echo 'test finished<br>';


try {
    $t = 3;
//    $t /= testExceeption(0);
    echo 'here t is';
} catch(Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
    var_dump($e);
}


/**
 * Function
 */
function myFun(&$x) {
    $x = $x * $x;
    return $x;
}

myFun($age);
echo $age;

$cars1 = ['Benz', 'BMW', 'Audi'];
$cars2 = array('benz', 'bmw');
//$array2 = array(1 => 'one_b', 3 => 'three_b', 4 => 'four_b');


//$cars1[]= 'VW';
//array_push($cars1, 'Volvo');
//array_pop($cars1);
//array_shift($cars1);
//

$cars3 = array_merge($cars1, $cars2);

//$cars3 = $cars1 + $array2;

//var_dump($cars1);
//echo '<br>';
//print_r($cars2);





//foreach($cars3 as $index => $car) {
//    echo '[' . $index . '] : ' . $car . '<br>';
//}
//echo count($cars1);







?>
</h1>

</body>
</html>

