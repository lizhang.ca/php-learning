<?php

/**
 * @param $current  file handle
 * @param $email    email to compared
 * @return bool true     email is found
 *              false    email is not found
 */

const ERR_CODE = 0x1000;
const ERR_INVALID_REQUEST_PARAMS =  ERR_CODE | 0x01;
const ERR_FILE_READ =  ERR_CODE | 0x02;

function filterEmail($current, $email) {
    if($current == null || $email == null) {return false;}

    $pos = strpos($current, $email);
    if ($pos !== false)
        return false;
    else return true;
}

if ($_POST) {

    $name = isset($_POST['name']) ? $_POST['name'] : false;
    $email = isset($_POST['email'])? $_POST['email'] : false;
    if(!$name || !$email)
        return ERR_INVALID_REQUEST_PARAMS;

    $res = json_encode(['name'=>$name, 'email'=>$email])  . PHP_EOL;

    $file = 'post.txt';
    $current = file_get_contents($file);

    $filter = filterEmail($current, $email);
    if ($filter){
        $current .= json_encode(['name'=>$name, 'email'=>$email]) . PHP_EOL;
        file_put_contents($file, $current);
        echo 'write to file';
    }
    else {
        echo 'already exists';
    }

}
//    $file = 'db.txt';
//    $current = fopen($file, 'a') or die("Failed to open file");
//    $tryWrite = fwrite($current, $res, strlen($res));
//    if($tryWrite !== FALSE) {
//        echo ' succeed';
//    } else {
//        echo ' failed';
//    }
//    fclose($current);
//    $current = null;
//
else {

    ?>

    <html>
        <head>
            <meta charset="UTF-8">
            <meta name="viewport"
                  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <title>Document</title>
        </head>
        <body>
            <form action="" method="post">
            Name: <input type="text" name="name"><br>
            E-mail: <input type="text" name="email"><br>
                <input type="submit">
            </form>
        </body>
        </html>

<?php
}
